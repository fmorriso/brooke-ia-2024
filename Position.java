import java.util.ArrayList;
import java.util.Arrays;

class Position
{
    private final static String[] images = {"wolf", "turtle", "rabbit", "dolphin", "parrot", "penguin", "cheetah", "frog"};
    private  ArrayList<Integer> positions = new ArrayList<Integer>();
    private  ArrayList<String> cards = new ArrayList<String>(Arrays.asList(images));
    private  ArrayList<String> assignments = new ArrayList<String>();

    private GameController controller;

    public Position(GameController controller)
    {
        this.controller = controller;
    }

    public  String getAssigment(int a)
    {
        String obj = assignments.get(a - 1);
        return obj;
    }

    public  void startPos()
    {
        for (int i = 1; i <= 16; i++) {
            positions.add(i);
        }
    }

    public  void setPos()
    {
        if (assignments == null || assignments.size() == 0) {
            assignments = new ArrayList<String>();
            for (int i = 0; i < 16; i++) {
                assignments.add(String.format("Assignment %d", i));
            }
        }

        int n = 0;

        for (String c : cards) {
            for (int p = 0; p < 2; p++) {
                n = (int) (Math.random() * positions.size());
                assignments.set(n, c);
                positions.remove(n);
            }
        }
    }


    // Create a method that sets card positions
    // Create an array that tracks card positions
    /* Create a method that checks when cards match and counts number of matches,
    and activates the end of the round when matches is equal to 8 */
    // 
}