public class Driver
{
    public static void main(String[] args)
    {
        System.out.format("Java version: %s%n", getJavaVersion());
        GameController controller = new GameController();
        Setup s  = new Setup(controller);
        s.runTheGameAfterConstructorHasFinished();
    }

    private static String getJavaVersion()
    {
        Runtime.Version runTimeVersion = Runtime.version();
        return String.format("%s.%s.%s.%s", runTimeVersion.feature(), runTimeVersion.interim(), runTimeVersion.update(), runTimeVersion.patch());
    }
}
