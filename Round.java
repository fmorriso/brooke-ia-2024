class Round
{
    private int currentScore = 1000;
    private int highScore = 0;
    private int matchesMade = 0;

    private GameController controller;
    private Position pos;
    private Setup setup;



    public Round(GameController controller)
    {
        this.controller = controller;
        this.pos = controller.getPos();
        this.setup = controller.getSetup();
    }

    public void incScore()
    {
        currentScore += 100;
    }

    public void incMatches()
    {
        matchesMade++;
    }

    public int getMatches()
    {
        return matchesMade;
    }

    public void decScore()
    {
        currentScore -= 20;
        if (currentScore < 0) {
            currentScore = 0;
        }
    }

    public void checkHighScore(int newScore)
    {
        if (newScore > highScore) {
            highScore = newScore;
        }
    }

    public int getCurrentScore()
    {
        return currentScore;
    }

    public int getHighScore()
    {
        return highScore;
    }

    public void endRound()
    {
        checkHighScore(currentScore);
        pos.startPos();
        pos.setPos();
        setup.resetAll();
        setup.setHighScore(this.getHighScore());
    }
}